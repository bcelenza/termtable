{EventEmitter} = require 'events'
spawn          = require('child_process').spawn
http           = require 'http'

class Player extends EventEmitter

  # initiates the player, creating the mplayer child process
  constructor: (@tt) ->
    # create the mplayer process
    @._init_process()

  # plays a song from a turntable.fm song object
  play: (song) ->
    # make the necessary HTTP calls to get the data
    self = @

    # get the direct audio URL from turntable
    options = 
      hostname: 'turntable.fm'
      port: 80
      path: "/getfile?client=iphone&roomid=#{self.tt.roomId}&userid=#{self.tt.userId}&fileid=#{song._id}"

    # the first request is made to turntable.fm to get a downloadable URL
    request = http.request options, (response) ->
      if response.statusCode != 302
        self.tt.logger.error "/getfile returned response #{response.statusCode} with headers #{JSON.stringify(response.headers)}"
        self.tt.window.body "Received /getfile response #{response.statusCode}", self.tt.window.colors.RED
      else
        self.tt.logger.debug "/getfile returned response #{response.statusCode} with headers #{JSON.stringify(response.headers)}"
        # the location of the audio file is in the response headers
        audio_path = response.headers['location']

        # play the song
        self._send_process "loadfile #{audio_path}"
        self.emit 'play', song

    # say the magic word
    request.setHeader('User-Agent', 'AppleCoreMedia/1.0.0.10B329 (iPhone; U; CPU OS 6_1_3 like Mac OS X; en_us)')
    request.end()    

  # stops playing
  stop: ->
    @tt.logger.debug 'Pausing audio'
    @process.stdin.write "pause\n"
    @.emit 'stop'

  # kills the process
  kill_process: ->
    @process.removeAllListeners()
    @process.kill()

  # toggles mute on or off
  toggle_mute: ->
    if @muted
      @tt.logger.debug "Unmuting sound"
      @._send_process "mute 0"
      @muted = false
      @.emit 'mute', false
    else
      @tt.logger.debug "Muting sound"
      @._send_process "mute 1"
      @muted = true
      @.emit 'mute', true

  # turns the volume up
  volume_up: ->
    @._send_process "volume 1"

  # turns the volume down
  volume_down: ->
    @._send_process "volume -1"


  # >> PRIVATE

  _init_process: ->
    self = @

    @process = spawn('mplayer', [
      # command line only
      '-nogui',
      # reduce logging output
      '-really-quiet',
      # don't close unless we tell you to
      '-idle',
      # slave mode: allow commands from STDIN
      '-slave'
    ])

    @process.on 'close', (code, signal) ->
      self.tt.logger.error "mplayer closed with code=#{code} signal=#{signal}"
      self.tt.window.body "Mplayer process died, please restart termtable.", self.tt.window.colors.RED

    # listen for potential issues and log them
    @process.stdout.on 'data', (data) ->
      self.tt.logger.debug "mplayer STDOUT: #{data}"

    @process.stderr.on 'data', (data) ->
      self.tt.logger.error "mplayer STDERR: #{data}"

  # sends a command to the process
  _send_process: (command) ->
    @process.stdin.write "#{command}\n"

module.exports = Player