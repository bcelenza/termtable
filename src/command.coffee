{EventEmitter} = require 'events'

class Command extends EventEmitter
  # returns the command this listener will respond to
  responds_to: ->

  # returns a brief description of what this command does
  desc: ->

  constructor: ->

  # trigger to respond to the command
  respond: (args, window, tt) ->


module.exports = Command