Listener = require '../listener'

class EscortListener extends Listener
  setup: (window, tt) ->
    tt.on 'escort', (data)->
      tt.logger.debug "'escort' event called"

      user_name = data.user[0].name
      mod_name = tt.users[data.modid].name
      tt.window.body "#{user_name} was kindly escorted off the stage by #{mod_name}", tt.window.colors.MAGENTA

module.exports = EscortListener