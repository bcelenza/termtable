Listener = require '../listener'

class EndsongListener extends Listener
  setup: (window, tt) ->
    tt.on 'endsong', (data) ->
      tt.logger.debug "'endsong' event called"

module.exports = EndsongListener