Listener = require '../listener'

class RoomchangeListener extends Listener
  setup: (window, tt) ->
    tt.on 'roomChanged', (data) ->
      tt.logger.debug "'roomChanged' event called"

      room = data.room.name
      song = data.room.metadata.current_song
      current_dj = data.room.metadata.current_dj

      # capture all the users
      tt.users = {}
      data.users.forEach (user) ->
        tt.users[user.userid] = user

      # begin playing the current song
      tt.player.play(song)    

      # set the title and body
      window.body "Joined #{room}", window.colors.YELLOW
      window.body "#{tt.users[current_dj].name} is playing '#{song.metadata.song}' by #{song.metadata.artist}", window.colors.CYAN
      window.title room
      window.song "'#{song.metadata.song}' by #{song.metadata.artist}", tt.users[current_dj].name

module.exports = RoomchangeListener