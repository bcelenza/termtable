Listener = require '../listener'

class DeregisterListener extends Listener
  setup: (window, tt) ->
    tt.on 'deregistered', (data)->
      tt.logger.debug "'deregistered' event called for user #{data.user[0].name}"
      user = data.user
      delete tt.users[user.userid]

module.exports = DeregisterListener