Listener = require '../listener'

class SpeakListener extends Listener
  setup: (window, tt) ->
    tt.on 'speak', (data) ->
      window.body "<#{data.name}> #{data.text}"

module.exports = SpeakListener