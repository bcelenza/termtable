Listener = require '../listener'

class NewsongListener extends Listener
  setup: (window, tt) ->
    tt.on 'newsong', (data) ->
      tt.logger.debug "'newsong' event called"

      song = data.room.metadata.current_song
      current_dj = data.room.metadata.current_dj
      tt.player.play(song)
      window.body "#{tt.users[current_dj].name} started playing '#{song.metadata.song}' by #{song.metadata.artist}", window.colors.CYAN
      window.song "'#{song.metadata.song}' by #{song.metadata.artist}", tt.users[current_dj].name

module.exports = NewsongListener