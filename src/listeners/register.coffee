Listener = require '../listener'

class RegisterListener extends Listener
  setup: (window, tt) ->
    tt.on 'registered', (data)->
      tt.logger.debug "'registered' event called for user #{data.user[0].name}"

      user = data.user
      tt.users[user.userid] = user

module.exports = RegisterListener