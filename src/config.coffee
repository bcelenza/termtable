fs = require 'fs'
{EventEmitter} = require 'events'

class Config extends EventEmitter
  # inits the Config, loading the config from the hard disk (blocking call)
  constructor: ->
    @config_path = "#{__dirname}/../config.json"
    contents = fs.readFileSync(@config_path)
    @hash = JSON.parse(contents)

  # serializes the data to the hard disk (blocking call)
  save: ->
    contents = JSON.stringify(@hash, null, '\t')
    fs.writeFileSync(@config_path, contents)

  # sets a given value for a given key in the config hash
  set: (key, value) ->
    @hash[key] = value

  # returns a value from the config hash
  get: (key) ->
    return @hash[key]

  # returns true if the provided key has a value
  is_set: (key) ->
    return true if @hash[key] != undefined and @hash[key] != ""
    return false

module.exports = Config