nc = require 'ncurses'
widgets = require "#{__dirname}/../node_modules/ncurses/lib/widgets"

{EventEmitter} = require 'events'

class Window extends EventEmitter
  # sets up the window layout
  constructor: (@tt) ->
    @root = new nc.Window()
    @main = new nc.Window(nc.lines, nc.cols)
    @main.scrollok(true)
    @main.hline(@main.height-2, 0, @main.width)
    @main.setscrreg(1, @main.height-3) # Leave one line at the top for the header
    @main.inbuffer = ""
    @._setup_input()
    @main.cursor(@main.height-1, 0)
    @current_title = ""
    @current_subtitle = ""

    # setup default color system
    @colors = 
      BLACK:   nc.colorPair(2, nc.colors.BLACK, 0)
      RED:     nc.colorPair(3, nc.colors.RED, 0)
      GREEN:   nc.colorPair(4, nc.colors.GREEN, 0)
      YELLOW:  nc.colorPair(5, nc.colors.YELLOW, 0)
      BLUE:    nc.colorPair(6, nc.colors.BLUE, 0)
      MAGENTA: nc.colorPair(7, nc.colors.MAGENTA, 0)
      CYAN:    nc.colorPair(8, nc.colors.CYAN, 0)
      WHITE:   nc.colorPair(9, nc.colors.WHITE, 0)
      TITLE:   nc.colorPair(10, nc.colors.WHITE, nc.colors.BLUE)

    # load the widgets
    @widgets = widgets

  # refreshes the main window
  refresh: ->
    @main.refresh()

  # cleans up ncurses output and exits the application
  cleanup: ->
    nc.cleanup()
    process.exit(0)

  # sets the window title
  title: (message) ->
    @current_title = message
    @._update_header()

  # sets the window subtitle
  song: (message, dj) ->
    @current_song = message
    @current_dj = dj
    @._update_header()

  # appends a new message to the body, with an optional color
  body: (message, color) ->
    # timestamp!
    time = new Date()
    hours = '' + time.getHours()
    mins = '' + time.getMinutes()
    hours = '0' + hours if hours.length == 1
    mins = '0' + mins if mins.length == 1
    timestamp = "#{hours}:#{mins}"

    # capture the current cursor location
    curx = @main.curx
    cury = @main.cury

    # scroll the window down 1
    @main.scroll(1)

    # set the cursor
    @main.cursor(@main.height - 3, 0)

    # print the timestamp
    @main.print("[ #{timestamp} ] ")

    # apply any colors
    @main.attron(color) if color

    # print the main message
    @main.print(message)

    # reset colors
    @main.attroff(color) if color

    # reset the cursor
    @main.cursor(cury, curx)

    # and refresh
    @main.refresh()

  # >> PRIVATE

  # sets up the input area of the window
  _setup_input: ->
    window = @

    # set the main event handler when a key is pressed
    @main.on 'inputChar', (char, charCode, isKey) ->
      self = @

      # escape key
      if charCode == nc.keys.ESC
        window.cleanup()
      # delete key
      else if charCode == nc.keys.DEL
        prev_x = self.curx
        self.delch(self.height-1, self.curx)
        self.inbuffer = self.inbuffer.substring(0, self.curx-1) + self.inbuffer.substring(self.curx)
        self.cursor(self.height-1, prev_x)

      # backspace key
      else if charCode == nc.keys.BACKSPACE or charCode == 127
        if self.curx > 0
          self.delch(self.height-1, self.curx-1)
          self.inbuffer = self.inbuffer.substring(0, self.curx) + self.inbuffer.substring(self.curx+1)

      # left key
      else if charCode == nc.keys.LEFT and self.curx > 0
        self.cursor(self.height-1, self.curx-1)
        
      # right key
      else if charCode == nc.keys.RIGHT and self.curx < self.inbuffer.length
        self.cursor(self.height-1, self.curx+1)

      # end key
      else if charCode == nc.keys.END
        self.cursor(self.height-1, self.inbuffer.length)

      # home key
      else if charCode == nc.keys.HOME
        self.cursor(self.height-1, 0)

      # enter/return key
      else if charCode == nc.keys.NEWLINE
        # emit the command event with the entered command
        command = self.inbuffer.trim()
        if command.length > 0
          window.emit('input', command)
          self.inbuffer = ''
          self.cursor(self.height-1, 0)
          self.clrtoeol()

      # up arrow
      else if charCode == nc.keys.UP
        window.tt.player.volume_up()

      # down arrow
      else if charCode == nc.keys.DOWN
        window.tt.player.volume_down()
      
      # any other character
      else if ((charCode > 32 and charCode < 126) or charCode == nc.keys.SPACE) and self.curx < self.width-1
        self.echochar(charCode)
        self.inbuffer += char

      # always refresh the window
      self.refresh()

  # updates the windows header bar with the latest title and subtitle
  _update_header: ->
    # capture the current cursor location
    curx = @main.curx
    cury = @main.cury

    # set the cursor to 0,0 and clear the line
    @main.cursor(0,0)
    @main.clrtoeol()

    # set the header
    @main.attron(nc.colorPair(10))

    header = "turntable.fm"
    header += " - #{@current_title}" if @current_title
    header += " (#{@current_song})" if @current_song
    header += " [DJ: #{@current_dj}]" if @current_dj

    @main.addstr(header, @main.width)

    @main.attroff(nc.colorPair(10))

    # reset the cursor back to its previous spot
    @main.cursor(cury, curx)

    # refresh
    @main.refresh()

module.exports = Window