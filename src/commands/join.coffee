Command = require "#{__dirname}/../command"

class JoinCommand extends Command
  responds_to: ->
    'join'

  desc: ->
    'Joins a room by id'

  respond: (args, window, tt) ->
    return window.body "No room ID specified", window.colors.RED if args.length == 0
    short_id = args[0]
    room = tt.rooms[short_id]

    tt.logger.debug "Joining room ID #{room.roomid} (#{room.name})"
    window.body "Joining #{room.name}", window.colors.YELLOW

    tt.roomRegister room.roomid
      
    

module.exports = JoinCommand

