Command = require "#{__dirname}/../command"

class MuteCommand extends Command
  responds_to: ->
    ['mute', 'm']

  desc: ->
    'Mutes/unmutes the audio'

  respond: (args, window, tt) ->
    tt.player.toggle_mute()


module.exports = MuteCommand