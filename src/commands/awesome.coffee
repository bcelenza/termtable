Command = require "#{__dirname}/../command"

class AwesomeCommand extends Command
  responds_to: ->
    ['awesome', 'a', 'bop']

  desc: ->
    'Gives the current song an up vote'

  respond: (args, window, tt) ->
    tt.vote 'up', (data) ->
      tt.logger.debug "'up' vote returned data: #{JSON.stringify(data)}"
      if data.success
        window.body "You voted: Awesome!", window.colors.GREEN
      else
        window.body "Error: #{data.err}", window.colors.RED


module.exports = AwesomeCommand