Command = require "#{__dirname}/../command"

class ListCommand extends Command
  responds_to: ->
    'list'

  desc: ->
    'Lists the top 20 rooms'

  respond: (args, window, tt) ->
    # get the top 20 rooms
    tt.listRooms 0, (data) ->

      window.body "Top 20 Rooms (type /join [id] to enter room):", window.colors.YELLOW
      for i in [0..data.rooms.length] by 1
        room = data.rooms[i][0]
        short_id = room.roomid.substring(0, 6)
        tt.rooms[short_id] = room
        window.body "[#{short_id}] #{room.name}", window.colors.YELLOW

      # widget_options = 
      #   title: 'Rooms'
      #   height: 30
      #   multi: false
      #   style: 
      #     colors:
      #       bg: 'blue'
      #       sel: 
      #         fg: 'red'
      # window.widgets.ListBox listed_rooms, widget_options, (selected) ->
      #   # refresh the window
      #   window.refresh()

      #   # stop the currently playing song
      #   tt.player.stop()
        
      #   # Join the new room
      #   room_index = listed_rooms.indexOf(selected)
      #   room = data.rooms[room_index][0]
      #   tt.logger.debug "Joining room ID #{room.roomid} (#{room.name})"
      #   tt.roomRegister room.roomid, (data) ->
      #     tt.logger.debug "roomRegister returned data #{JSON.stringify(data)}"
      
    

module.exports = ListCommand

