Command = require "#{__dirname}/../command"

class RoomsearchCommand extends Command
  responds_to: ->
    ['roomsearch', 'rs']

  desc: ->
    'Searches for a room by name'

  respond: (args, window, tt) ->
    return window.body "No search specified", window.colors.RED if args.length == 0

    # rejoin the args
    query = args.join ' '

    options =
      limit: 20
      query: query
    tt.searchRooms options, (data) ->
      window.body "Search results (type /join [id] to enter room):", window.colors.YELLOW
      for i in [0..data.rooms.length] by 1
        room = data.rooms[i][0]
        short_id = room.roomid.substring(0, 6)
        tt.rooms[short_id] = room
        window.body "[#{short_id}] #{room.name}", window.colors.YELLOW

      
    

module.exports = RoomsearchCommand

