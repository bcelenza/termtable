Command = require "#{__dirname}/../command"

class LameCommand extends Command
  responds_to: ->
    ['lame', 'l']

  desc: ->
    'Gives the current song a down vote'

  respond: (args, window, tt) ->
    tt.vote 'down', (data) ->
      tt.logger.debug "'down' vote returned data: #{JSON.stringify(data)}"
      if data.success
        window.body "You voted: Lame!", window.colors.RED
      else
        window.body "Error: #{data.err}", window.colors.RED


module.exports = LameCommand