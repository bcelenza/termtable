Command = require "#{__dirname}/../command"

class HelpCommand extends Command
  responds_to: ->
    'help'

  desc: ->
    'Shows this help'

  respond: (args, window, tt) ->
    window.body 'Available commands:'
    # list out all commands
    for responds_to, command of tt.command_map
      window.body "/#{responds_to} - #{command.desc()}"


module.exports = HelpCommand