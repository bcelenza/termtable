Command = require "#{__dirname}/../command"

class QuitCommand extends Command
  responds_to: ->
    ['quit', 'q']

  desc: ->
    'Exits the application'

  respond: (args, window, tt) ->
    process.exit()

module.exports = QuitCommand