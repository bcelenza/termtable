fs        = require 'fs'

Bot       = require 'ttapi'
Log       = require 'log'

Window    = require './window'
Player    = require './player'

class TT extends Bot
  constructor: (config, runlevel='ERROR') ->
    self = @

    # check for proper config
    throw new Error("No auth found, did you configure it?") if !config.is_set('auth')
    throw new Error("No userId found, did you configure it?") if !config.is_set('userId')
    
    @window = new Window(self)
    @player = new Player(self)

    # containers for data
    @users = {} # users in the current room (id => user_object)
    @rooms = {}

    # setup the logger
    @logger = new Log(runlevel, fs.createWriteStream("#{__dirname}/../logs/termtable.log"))
    @logger.info 'TT Initializing...'

    # setup the command handlers
    commands_dir = "#{__dirname}/commands"
    @command_map = {}
    commands = fs.readdirSync(commands_dir)
    commands.forEach (item) ->
      Command = require "#{commands_dir}/#{item}"
      command = new Command()
      self.logger.debug "Registering command(s) #{command.responds_to()}"
      # allow command objects to respond to multiple / commands
      responders = command.responds_to()
      if typeof responders == 'string'
        responders = [responders]
      responders.forEach (responder) ->
        self.command_map[responder] = command

    # wire the event listeners
    listeners_dir = "#{__dirname}/listeners"
    listeners = fs.readdirSync(listeners_dir)
    listeners.forEach (item) ->
      Listener = require "#{listeners_dir}/#{item}"
      listener = new Listener()
      listener.setup(self.window, self)
    
    # init the ttapi bot
    @window.title "Connecting..."
    @window.body "Connecting to turntable.fm..."
    self.on 'ready', () ->
      self.logger.debug "'ready' event called"
      self.window.body "Connected."
      self.command '/list' if !config.is_set('roomId')

    super(config.get('auth'), config.get('userId'), config.get('roomId'))

    # setup the main input loop
    @window.on 'input', (input) ->
      # if it doesn't begin with a slash, it's chat
      if input.charAt(0) != '/'
        self.speak(input)
      else
        self.command(input)


  # executes a command based on user input
  command: (input) ->
    command_pattern = /^\/([^\s]+)(\s(.+))?/
    command_pieces = command_pattern.exec(input)
    input_command = command_pieces[1]
    input_args = []
    input_args = command_pieces[3].split(' ') if command_pieces[3] != undefined

    try
      Command = @command_map[input_command]
      Command.respond(input_args, @window, @)
    catch e
      @logger.warning "Command error: #{e.message}"
      @window.body "Unknown command or error: #{input_command}", @window.colors.RED

module.exports = TT