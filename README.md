Termtable brought turntable.fm to your terminal. Since turntable.fm is now offline
permanently, this repository serves only as a historical record of the functionality
and reverse engineering of the API.

# Usage

## Why use it?

Termtable provided a decent alternative to keeping your browser open all day.
If you were prone to accidentally quitting out of the tab, or just didn't want to
keep the browser open, or like using terminal for simple things like playing 
music, then this would have been for you.

## Can I use this to capture/record/save music?

NO! This application in no way captures the audio to be saved to disk. Support 
struggling/new/talented artists. Pay for your music. $.99 won't kill you.

# Installation

First things first, this only works on Macs/Unix currently. Sorry Windows 
users. If this angers you, fork it and fix it.

You'll also need mplayer installed, and available in PATH. This allows playing 
music from the command line via STDIN, and does quite a good job of it.

For Mac users (with homebrew):

    $ brew install mplayer

For Mac users (with MacPorts):

    $ port install mplayer

For Ubuntu users:

    $ apt-get install mplayer

All others: Check your respective package manager (yum, pacman, etc.). You'll 
likely need to [compile from source](http://www.mplayerhq.hu/).

Once you're all ready, just install via npm:

    $ npm install -g termtable

You'll need to configure some things, such as your userId, and auth token. You 
can get that information here: 
[http://alaingilbert.github.io/Turntable-API/bookmarklet.html](http://alaingilbert.github.io/Turntable-API/bookmarklet.html)

Once you have it, just pass it in the command on first run

    $ termtable --auth auth+live+846a09bdf85h2205bf506de3547fcc53865e0248 --userid 4e2da557a7df5151481034db

__This only needs to be done once__ (or if you want to change users). Subsequent 
runs can be done easily with just

    $ termtable

# Extending

## Basics

Termtable extension is separated into two distinct areas:

* Commands
* Listeners

Commands respond to a specific command from user input (starting with a /). 
Listeners listen and respond to events from the Turntable API bot.

Both classes are simple to implement and extend EventEmitter. Both classes also
have the power to use and manipulate the Window and TT classes.

The window class is a simple class that works with node-ncurses to provide the 
inteface.

The TT class extends [alaingilbert's](https://github.com/alaingilbert)
[Turntable-API (ttapi)](https://github.com/alaingilbert/Turntable-API) to 
provide it with some additional functionality, such as the music player itself.

By default, termtable will only log error conditions to logs/termtable.log. 
You can enable debug loggin by specifying --debug when launching termtable.

    $ termtable --debug

## Commands

Coming soon. See src/command.coffee for the base class, and examples in 
src/commands/

## Listeners

Coming soon. See src/listener.coffee for the base class, and examples in 
src/listeners/

# Dependencies (Thank You)

Thanks to the following libraries for making my life a whole lot easier.

* [coffee-script](https://github.com/jashkenas/coffee-script)
* [optparse](https://github.com/jfd/optparse-js)
* [ttapi](https://github.com/alaingilbert/Turntable-API)
* [ncurses](https://github.com/mscdex/node-ncurses)
* [log](https://github.com/visionmedia/log.js)

# License

By installing or using this library in any way you agree to the following.

The MIT License (MIT)
Copyright (c) 2013 Brian J. Celenza <bcelenza@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.